const userservice = {
  userList: [
    {
      id: 1,
      name: 'Pakawat K',
      gender: 'M',
      birthday: '1989-02-01',
      position: 'Dev',
      email: 'test1@hotmail.com'
    },
    {
      id: 2,
      name: 'Threerawen W',
      gender: 'M',
      birthday: '1990-11-04',
      position: 'SA',
      email: 'test2@hotmail.com'
    }
  ],
  lastId: 3,
  addUser (user) {
    user.id = this.lastId++
    this.userList.push(user)
  },
  updateUser (user) {
    const index = this.userList.findIndex(item => item.id === user.id)
    this.userList.splice(index, 1, user)
  },
  deleteUser (user) {
    const index = this.userList.findIndex(item => item.id === user.id)
    this.userList.splice(index, 1)
  },
  getUsers () {
    return [...this.userList]
  }
}
export default userservice
